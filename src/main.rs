#[macro_use] extern crate serde_derive;
extern crate chrono;
extern crate serenity;
extern crate toml;

use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::process::exit;
use std::thread;
use std::time;
use std::time::SystemTime;

use chrono::{DateTime, Duration, Timelike, Utc};
use chrono::naive::NaiveTime;
use serenity::client::Client;
use serenity::model::id::ChannelId;
use serenity::prelude::EventHandler;

const CONFIG_FILE: &'static str =  "arua-helper.toml";


#[derive(Debug, Deserialize)]
struct Config {
    token: String,
    channels: Vec<u64>,
}

struct Handler;

impl EventHandler for Handler {}


fn main() {
    if !Path::new(CONFIG_FILE).exists() {
        eprintln!("Couldn't find config file: {}", CONFIG_FILE);
        exit(1);
    }

    let mut file = File::open(CONFIG_FILE).map_err(|err| {
        eprintln!("Couldn't open config file: {}", CONFIG_FILE);
        eprintln!("Error: {:?}", err);
        exit(1);
    }).unwrap();

    let mut buffer = String::new();
    file.read_to_string(&mut buffer).map_err(|err| {
        eprintln!("Couldn't read config file: {}", CONFIG_FILE);
        eprintln!("Error: {:?}", err);
        exit(1);
    }).unwrap();

    let config: Config = toml::from_str(&buffer).map_err(|err| {
        eprintln!("Error parsing config file: {}", CONFIG_FILE);
        eprintln!("Error: {:?}", err);
        exit(1);
    }).unwrap();

    // Opens websocket connection with discord server
    let mut client = Client::new(&config.token, Handler).unwrap();

    thread::spawn(move || {
        client.start();
    });

    let jones1 = NaiveTime::from_hms(17, 50, 0);
    let jones2 = NaiveTime::from_hms(1, 50, 0);

    let mut send_warning = true;
    let mut send_start = true;

    let jones_warning_msg = "Jones quest is starting in 10 minutes";
    let jones_start_msg = "Jones quest is starting";

    let channel_ids: Vec<ChannelId> = config.channels
        .iter()
        .map(|channel_id| ChannelId(*channel_id))
        .collect();

    loop {
        let now = DateTime::<Utc>::from(SystemTime::now());

        let time_to_jones1 = jones1 - now.time();
        let time_to_jones2 = jones2 - now.time();

        let min_to_jones1 = time_to_jones1.num_minutes();
        let min_to_jones2 = time_to_jones2.num_minutes();

        let sec_to_jones1 = time_to_jones1.num_seconds();
        let sec_to_jones2 = time_to_jones2.num_seconds();

        // Send warning
        if min_to_jones1 == 10 || min_to_jones2 == 10 {
            if send_warning {
                for channel in &channel_ids {
                    channel.say(jones_warning_msg);
                }
                println!("{}", jones_warning_msg);
                send_warning = false; // Only send the warning once
            }
        } else {
            send_warning = true;
        }

        // Send start
        if  (min_to_jones1 == 0 && sec_to_jones1 <= 10) ||
            (min_to_jones2 == 0 && sec_to_jones2 <= 10) {
            if send_start {
                for channel in &channel_ids {
                    channel.say(jones_start_msg);
                }
                println!("{}", jones_start_msg);
                send_start = false;
            }
        } else {
            send_start = true;
        }

        thread::sleep(time::Duration::from_secs(1));
    }
}
